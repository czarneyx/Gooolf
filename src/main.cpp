#include <iostream>
#include <vector>
#include <cmath>
#include <string>
#include <stdlib.h>
#include <time.h>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

enum Difficulty {

    Easy = 0,
    Medium = 1,
    Hard = 2

};

enum Mode {

    Menu = 0,
    Options = 1,
    Pause = 2,
    Game = 3,
    Arcade = 4,
    EndGame = 5

};

using namespace sf;

const int WIDTH = 640;
const int HEIGHT = 480;
const int BORDER = 40;
const int MAX_FPS = 240;

const int GRID_SIZE_X = 20;
const int GRID_SIZE_Y = 15;
const int TILE_SIZE_X = 32;
const int TILE_SIZE_Y = 32;

const int SFX_VOLUME = 30;
const int MUSIC_VOLUME = 20;

const float HOLE_ANIMATION_TIME = 2.25;

const int MAX_VELOCITY = 450;
const int VELOCITY_LIMIT_TO_HOLE = 275;
const float BALL_VELOCITY = 1;
const int FRICTION = 150;

const int SHADOWS_OFFSET_Y = 3;
const int SHADOWS_OFFSET_X = 2;
const int POWERMETER_OFFSET_X = 50;
const int POWERMETER_OFFSET_Y = -15;

Difficulty difficulty = Difficulty::Easy;
Mode mode = Mode::Menu;

RenderWindow window(VideoMode(WIDTH+2*BORDER, HEIGHT+2*BORDER), "Gooolf", Style::Titlebar | Style::Close);

Color colorEasy = Color(100, 255, 100, 255);
Color colorMedium = Color(255, 255, 100, 255);
Color colorHard = Color(255, 100, 100, 255);
Color colorShadow = Color(60, 60, 60, 220);
Color colorOverlayedText = Color(93, 232, 158, 255);
Color colorText = Color::White;

SoundBuffer swingBuffer;
SoundBuffer chargeBuffer;
SoundBuffer holeBuffer;
SoundBuffer clickBuffer;
Sound swingSound;
Sound chargeSound;
Sound holeSound;
Sound clickSound;
Music music;

Font font;
std::vector<Text> menuTexts;
std::vector<Text> menuTextsShadow;
std::vector<Text> guiTexts;
std::vector<Text> guiTextsShadow;
std::vector<Text> pauseTexts;
std::vector<Text> pauseTextsShadow;
std::vector<Text> endGameTexts;
std::vector<Text> endGameTextsShadows;
std::vector<Text> OptionsTexts;
std::vector<Text> OptionsTextsShadows;

Texture ballTexture;
Texture ballShadowTexture;
Texture arrowTexture;
Texture bgTexture;
Texture powerMeterFgTexture;
Texture powerMeterBgTexture;
Texture powerMeterOvTexture;
Texture tileTexture;
Texture holeTexture;
Texture pauseScreenOverlayTexture;

Sprite ball;
Sprite ballShadow;
Sprite arrow;
Sprite bg;
Sprite powerMeterFg;
Sprite powerMeterBg;
Sprite powerMeterOv;
Sprite tile;
Sprite hole;
Sprite pauseScreenOverlay;

std::vector<std::vector<Sprite>> levelTiles;
std::vector<Sprite> currentLevel;

std::vector<Vector2i> ballPositions;
std::vector<Vector2i> holePositions;

Vector2i ballPosition;
Vector2i holePosition;

void Init();
void InitWindow();
void InitSound();
void InitTexts();
void InitTextures();
void InitSprites();
void InitLevels();

void GenerateLevel1();
void GenerateLevel2();
void GenerateLevel3();
void GenerateLevel4();

void InitMenuTexts();
void InitGuiTexts();
void InitPauseTexts();
void InitEndGameTexts();
void InitOptionsTexts();

void MuteMusic();
void MuteSounds();
void UnmuteMusic();
void UnmuteSounds();

void LoadLevel(int _level, int &_strokes);
void LoadArcadeMode(int &_strokes, int streak);
void UpdateArcadeMode(int _strokes, int &_lastHoleStrokes, int &_streak);
void UpdateTexts(int _levelStreak, int _strokes, bool _arcadeMode = false);

void UpdateClock(Clock _clock, int &_seconds, int &_minutes);
void BallIsMoving(Vector2i _mouseFinalPosition, Vector2i _initialBallPosition, float _deltaTime, Vector2i &_direction, float &_velocity, bool &_isMoving, int _level, bool &_inHole, int &_holeTime, Clock _clock, float &_holeAngle, Vector2i _fixedPoint);

void PressedOnTheBall(Vector2i &_initialBallPosition,bool &_isButtonDown);
void PressedOnTheDifficultyText();
void ReleaseFromTheBall(bool &_isButtonDown, Vector2i &_mouseFinalPosition, Vector2i &_mousePosition, Vector2i &_direction, bool &_isMoving, float &_velocity, int &_strokes, bool _muteSfx, Vector2i _initialBallPosition, Vector2i &_fixedPosition);
void HoldingTheButton(float &_velocity, Vector2i _mousePosition, Vector2i _initialBallPosition);

void MoveTheBall(Vector2i _mouseFinalPosition, Vector2i _initialBallPosition, float _deltaTime, Vector2i &_direction, float &_velocity, bool &_isMoving, Vector2i _fixedPosition);
void BallAnimation(float _deltaTime, float _holeAngle);

void CalculateCollisionsWithScreenBounds(Vector2i &_direction);
void CalculateCollisionsWithTiles(int _level, Vector2i &_direction);
void CalculateCollisionsWithHole(bool &_inHole, int &_holeTime, Clock _clock, float &_velocity, float &_holeAngle);

void OverlayText(Vector2i _mousePosition);
void UpdateRender(int _level, bool isButtonDown, bool _isMoving, Mode _lastMode);

int RandomInt(int _max, int _min);
float GetDistance(Vector2i _vector1, Vector2i _vector2);
float GetAngle(Vector2i _vector1, Vector2i _vector2);
float DistanceToLine(Vector2i line_start, Vector2i line_end, Vector2i point);

int main(int argc, char **argv) {

    Clock clock;
    Clock deltaTimeClock;
    float deltaTime;

    bool isButtonDown = false;
    bool isMoving = false;
    Vector2i mouseFinalPosition;
    Vector2i initialBallPosition;
    Vector2i direction;
    Vector2i fixedPoint;
    float velocity = 0;

    int level = 0;
    int strokes = 0;
    int lastHoleStroke = 0;
    int streak = 0;

    int seconds = 0;
    int minutes = 0;

    float holeAngle;

    bool inHole = false;
    int holeTime = 0;

    bool muteSfx = false;
    bool muteMusic = false;

    Mode lastPlayedMode;
    Text lastOverlayedText;

    Init();

    // Main window loop
    while(window.isOpen()) {

        // Variables
        Vector2i mousePosition = Mouse::getPosition(window);
        Event event;

        deltaTime = deltaTimeClock.restart().asSeconds();

        UpdateClock(clock, seconds, minutes);

        // Checking events
        while(window.pollEvent(event)) {

            if(event.type == Event::Closed) window.close();

            if(event.type == Event::KeyPressed)
                if(event.key.code == Keyboard::Escape) {

                    if(mode == Mode::Menu) window.close();
                    else if((mode == Mode::Game || mode == Mode::Arcade) && !inHole) {

                        lastPlayedMode = mode;
                        mode = Mode::Pause;

                    }
                    else if(mode == Mode::Options) mode = Mode::Menu;
                    else if(mode == Mode::Pause) mode = lastPlayedMode;

                    clickSound.play();

                }

            if(event.type == Event::MouseButtonPressed)
                if(event.key.code == Mouse::Left) {

                    if(mode == Mode::Game || mode == Mode::Arcade) {

                        if(ball.getGlobalBounds().contains(mousePosition.x, mousePosition.y))
                            if(!isMoving) PressedOnTheBall(initialBallPosition, isButtonDown);

                        if(guiTexts[2].getGlobalBounds().contains(mousePosition.x, mousePosition.y)) PressedOnTheDifficultyText();

                    }
                    else if(mode == Mode::Menu) {

                        if(menuTexts[0].getGlobalBounds().contains(mousePosition.x, mousePosition.y)) {

                            mode = Mode::Game;
                            lastPlayedMode = Mode::Game;
                            clock.restart();
                            clickSound.play();
                            LoadLevel(level, strokes);

                        } 
                        else if(menuTexts[1].getGlobalBounds().contains(mousePosition.x, mousePosition.y)) {

                            mode = Mode::Arcade;
                            lastPlayedMode = Mode::Arcade;
                            clock.restart();
                            clickSound.play();
                            LoadArcadeMode(strokes, streak);

                        }
                        else if(menuTexts[2].getGlobalBounds().contains(mousePosition.x, mousePosition.y)) {

                            mode = Mode::Options;
                            clickSound.play();

                        }
                        else if(menuTexts[3].getGlobalBounds().contains(mousePosition.x, mousePosition.y)) window.close();

                    }

                    else if(mode == Mode::Pause) {

                        if(pauseTexts[0].getGlobalBounds().contains(mousePosition.x, mousePosition.y)) {

                            mode = lastPlayedMode;
                            clickSound.play();

                        }
                        
                        else if(pauseTexts[1].getGlobalBounds().contains(mousePosition.x, mousePosition.y)) {
                         
                            mode = Mode::Menu;
                            velocity = 0;
                            clickSound.play();
                        
                        }

                        else if(pauseTexts[2].getGlobalBounds().contains(mousePosition.x, mousePosition.y)) window.close();

                    }

                    else if(mode == Mode::EndGame) {

                        if(endGameTexts[2].getGlobalBounds().contains(mousePosition.x, mousePosition.y)) { 
                         
                            mode = Mode::Menu;
                            level = 0;
                            clickSound.play();

                        }
                        else if(endGameTexts[3].getGlobalBounds().contains(mousePosition.x, mousePosition.y)) window.close();

                    }

                    else if(mode == Mode::Options) {

                        if(OptionsTexts[0].getGlobalBounds().contains(mousePosition.x, mousePosition.y)) { 
                         
                            muteSfx = !muteSfx;
                            if(muteSfx) {

                                OptionsTexts[0].setColor(Color::Red);
                                MuteSounds();

                            }
                            else {

                                OptionsTexts[0].setColor(colorText);
                                UnmuteSounds();

                            }
                            clickSound.play();

                        }
                        if(OptionsTexts[1].getGlobalBounds().contains(mousePosition.x, mousePosition.y)) { 
                         
                            muteMusic = !muteMusic;
                            if(muteMusic) {

                                OptionsTexts[1].setColor(Color::Red);
                                MuteMusic();

                            }
                            else {

                                OptionsTexts[1].setColor(colorText);
                                UnmuteMusic();

                            }
                            clickSound.play();

                        }
                        if(OptionsTexts[2].getGlobalBounds().contains(mousePosition.x, mousePosition.y)) { 
                         
                            mode = Mode::Menu;
                            level = 0;
                            clickSound.play();

                        }

                    }

                }

            if (event.type == Event::MouseButtonReleased)
                if (event.key.code == Mouse::Left)  
                    if(mode == Mode::Game || mode == Mode::Arcade)
                        if(isButtonDown)
                            if(!isMoving) ReleaseFromTheBall(isButtonDown, mouseFinalPosition, mousePosition, direction, isMoving, velocity, strokes, muteSfx, initialBallPosition, fixedPoint);
        }

        // Is olding the mouse button
        if(isButtonDown) HoldingTheButton(velocity, mousePosition, initialBallPosition);

        // The ball is moving
        else if(isMoving && (mode == Mode::Game || mode == Mode::Arcade)) BallIsMoving(mouseFinalPosition, initialBallPosition, deltaTime, direction, velocity, isMoving, level, inHole, holeTime, clock, holeAngle, fixedPoint);

        if(inHole) BallAnimation(deltaTime, holeAngle);

        if(clock.getElapsedTime().asSeconds() > holeTime+HOLE_ANIMATION_TIME && inHole) {   // Level up

            if(mode == Mode::Game) {

                level++;

                LoadLevel(level, strokes);

                inHole = false;

            }
            else {

                UpdateArcadeMode(strokes, lastHoleStroke, streak);

                inHole = false;

            }

        }

        OverlayText(mousePosition);

        UpdateRender(level, isButtonDown, isMoving, lastPlayedMode);

    }

    return EXIT_SUCCESS;

}

void Init() {

    srand(time(NULL));
    InitWindow();
    InitSound();
    InitTexts();
    InitTextures();
    InitSprites();
    InitLevels();

}

void InitWindow() {

    window.setPosition(sf::Vector2i(1920/2-(WIDTH+2*BORDER)/2, (1080/2-(+2*BORDER))/2));
    window.setFramerateLimit(MAX_FPS);

}

void InitSound() {

    swingBuffer.loadFromFile("res/sfx/swing.wav");
    chargeBuffer.loadFromFile("res/sfx/charge.wav");
    holeBuffer.loadFromFile("res/sfx/hole.wav");
    clickBuffer.loadFromFile("res/sfx/click.wav");

    swingSound.setBuffer(swingBuffer);
    chargeSound.setBuffer(chargeBuffer);
    holeSound.setBuffer(holeBuffer);
    clickSound.setBuffer(clickBuffer);

    chargeSound.setVolume(SFX_VOLUME/1.5);
    holeSound.setVolume(SFX_VOLUME);
    clickSound.setVolume(SFX_VOLUME);

    music.openFromFile("res/music/bg_music.wav");
    music.setVolume(MUSIC_VOLUME);
    music.setLoop(true);
    music.play();

}

void InitTexts() {

    font.loadFromFile("res/font/font.ttf");

    InitMenuTexts();
    InitGuiTexts();
    InitEndGameTexts();
    InitPauseTexts();
    InitOptionsTexts();

}

void InitGuiTexts() {

    // Create the text
    Text text;
    text.setFont(font);
    text.setColor(colorText);

    text.setString("Hole/Streak XX");
    text.setPosition(BORDER, 0);
    guiTexts.push_back(text);

    text.setString("Strokes XX");
    text.setPosition(WIDTH+BORDER-125, 0);
    guiTexts.push_back(text);
    
    text.setString("Difficulty $DIFF");
    text.setPosition(BORDER, HEIGHT+BORDER);
    text.setColor(((difficulty == Difficulty::Easy) ? (colorEasy) : ((difficulty == Difficulty::Medium) ? (colorMedium) : (colorHard))));
    guiTexts.push_back(text);

    text.setString("Time XX:XX");
    text.setPosition(WIDTH+BORDER-125, HEIGHT+BORDER);
    text.setColor(colorText);
    guiTexts.push_back(text);

    text.setColor(colorShadow);
    // Text shadows
    text.setString("Hole/Streak XX");
    text.setPosition(BORDER+SHADOWS_OFFSET_X, 0+SHADOWS_OFFSET_Y);
    guiTextsShadow.push_back(text);

    text.setString("Strokes XX");
    text.setPosition(WIDTH+BORDER-125+SHADOWS_OFFSET_X, 0+SHADOWS_OFFSET_Y);
    guiTextsShadow.push_back(text);

    text.setString("Difficulty $DIFF");
    text.setPosition(BORDER+SHADOWS_OFFSET_X, HEIGHT+BORDER+SHADOWS_OFFSET_Y);
    guiTextsShadow.push_back(text);

    text.setString("Time XX:XX");
    text.setPosition(WIDTH+BORDER-125+SHADOWS_OFFSET_X, HEIGHT+BORDER+SHADOWS_OFFSET_Y);
    text.setColor(colorShadow);
    guiTextsShadow.push_back(text);

}

void InitMenuTexts() {

    Text text;

    text.setColor(colorText);
    text.setFont(font);
    text.setCharacterSize(65);

    text.setString("Play");
    text.setPosition(BORDER * 2, BORDER * 2);
    menuTexts.push_back(text);

    text.setString("Arcade");
    text.setPosition(BORDER * 2, BORDER * 4);
    menuTexts.push_back(text);

    text.setString("Options");
    text.setPosition(BORDER * 2, BORDER * 6);
    menuTexts.push_back(text);

    text.setString("Quit");
    text.setPosition(BORDER * 2, BORDER * 8);
    menuTexts.push_back(text);

    // Shadows

    text.setColor(colorShadow);

    text.setString("Play");
    text.setPosition(BORDER * 2+SHADOWS_OFFSET_X*2, BORDER * 2+SHADOWS_OFFSET_Y*2);
    menuTextsShadow.push_back(text);

    text.setString("Arcade");
    text.setPosition(BORDER * 2+SHADOWS_OFFSET_X*2, BORDER * 4+SHADOWS_OFFSET_Y*2);
    menuTextsShadow.push_back(text);

    text.setString("Options");
    text.setPosition(BORDER * 2+SHADOWS_OFFSET_X*2, BORDER * 6+SHADOWS_OFFSET_Y*2);
    menuTextsShadow.push_back(text);

    text.setString("Quit");
    text.setPosition(BORDER * 2+SHADOWS_OFFSET_X*2, BORDER * 8+SHADOWS_OFFSET_Y*2);
    menuTextsShadow.push_back(text);

}

void InitPauseTexts() {

    // Create the text
    Text text;

    text.setFont(font);
    text.setColor(colorText);
    text.setCharacterSize(65);

    text.setString("Resume");
    text.setPosition(BORDER * 2, BORDER * 2);
    pauseTexts.push_back(text);

    text.setString("Menu");
    text.setPosition(BORDER * 2, BORDER * 4);
    pauseTexts.push_back(text);

    text.setString("Quit");
    text.setPosition(BORDER * 2, BORDER * 6);
    pauseTexts.push_back(text);

    // Shadows

    text.setColor(colorShadow);

    text.setString("Resume");
    text.setPosition(BORDER * 2+SHADOWS_OFFSET_X*2, BORDER * 2+SHADOWS_OFFSET_Y*2);
    pauseTextsShadow.push_back(text);

    text.setString("Menu");
    text.setPosition(BORDER * 2+SHADOWS_OFFSET_X*2, BORDER * 4+SHADOWS_OFFSET_Y*2);
    pauseTextsShadow.push_back(text);

    text.setString("Quit");
    text.setPosition(BORDER * 2+SHADOWS_OFFSET_X*2, BORDER * 6+SHADOWS_OFFSET_Y*2);
    pauseTextsShadow.push_back(text);

}

void InitEndGameTexts() {

    // Create the text
    Text text;
    text.setFont(font);

    text.setColor(colorText);

    text.setString("Thanks");
    text.setCharacterSize(125);
    text.setPosition(130+BORDER*3/2, BORDER );
    endGameTexts.push_back(text);

    text.setCharacterSize(65);

    text.setString("for playing");
    text.setPosition(130+BORDER * 5/2, BORDER * 4);
    endGameTexts.push_back(text);

    text.setString("Menu");
    text.setPosition(212+BORDER * 2, BORDER * 7);
    endGameTexts.push_back(text);

    text.setString("Quit");
    text.setPosition(230+BORDER * 2, BORDER * 9);
    endGameTexts.push_back(text);

    // Shadows

    text.setColor(colorShadow);

    text.setString("Thanks");
    text.setCharacterSize(125);
    text.setPosition(130+BORDER*3/2+SHADOWS_OFFSET_X*2, BORDER+SHADOWS_OFFSET_Y*2);
    endGameTextsShadows.push_back(text);

    text.setCharacterSize(65);
    
    text.setString("for playing");
    text.setPosition(130+BORDER * 5/2 +SHADOWS_OFFSET_X*2, BORDER * 4 +SHADOWS_OFFSET_Y*2);
    endGameTextsShadows.push_back(text);

    text.setString("Menu");
    text.setPosition(212+BORDER * 2+SHADOWS_OFFSET_X*2, BORDER * 7+SHADOWS_OFFSET_Y*2);
    endGameTextsShadows.push_back(text);

    text.setString("Quit");
    text.setPosition(230+BORDER * 2+SHADOWS_OFFSET_X*2, BORDER * 9+SHADOWS_OFFSET_Y*2);
    endGameTextsShadows.push_back(text);

}

void InitOptionsTexts() {

    Text text;

    text.setColor(colorText);
    text.setFont(font);
    text.setCharacterSize(65);

    text.setString("Mute sound effects");
    text.setPosition(BORDER * 2, BORDER * 2);
    OptionsTexts.push_back(text);

    text.setString("Mute music");
    text.setPosition(BORDER * 2, BORDER * 4);
    OptionsTexts.push_back(text);

    text.setString("Menu");
    text.setPosition(BORDER * 2, BORDER * 6);
    OptionsTexts.push_back(text);

    // Shadows

    text.setColor(colorShadow);

    text.setString("Mute sound effects");
    text.setPosition(BORDER * 2+SHADOWS_OFFSET_X*2, BORDER * 2+SHADOWS_OFFSET_Y*2);
    OptionsTextsShadows.push_back(text);

    text.setString("Mute music");
    text.setPosition(BORDER * 2+SHADOWS_OFFSET_X*2, BORDER * 4+SHADOWS_OFFSET_Y*2);
    OptionsTextsShadows.push_back(text);

    text.setString("Menu");
    text.setPosition(BORDER * 2+SHADOWS_OFFSET_X*2, BORDER * 6+SHADOWS_OFFSET_Y*2);
    OptionsTextsShadows.push_back(text);

}

void MuteMusic() {

    music.setVolume(0);

}

void MuteSounds() {

    chargeSound.setVolume(0);
    holeSound.setVolume(0);
    clickSound.setVolume(0);

}

void UnmuteMusic() {

    music.setVolume(MUSIC_VOLUME);

}

void UnmuteSounds() {

    chargeSound.setVolume(SFX_VOLUME/1.5);
    holeSound.setVolume(SFX_VOLUME);
    clickSound.setVolume(SFX_VOLUME);

}

void InitTextures() {

    ballTexture.loadFromFile("res/gfx/ball.png");
    ballShadowTexture.loadFromFile("res/gfx/ball_shadow.png");
    arrowTexture.loadFromFile("res/gfx/arrow.png");
    bgTexture.loadFromFile("res/gfx/bg.png");
    powerMeterFgTexture.loadFromFile("res/gfx/powermeter_fg.png");
    powerMeterBgTexture.loadFromFile("res/gfx/powermeter_bg.png");
    powerMeterOvTexture.loadFromFile("res/gfx/powermeter_ov.png");
    tileTexture.loadFromFile("res/gfx/tile32_light.png");
    holeTexture.loadFromFile("res/gfx/hole.png");
    pauseScreenOverlayTexture.loadFromFile("res/gfx/pausescreen_overlay.png");

}

void InitSprites() {

    ball = Sprite(ballTexture);
    ballShadow = Sprite(ballShadowTexture);
    arrow = Sprite(arrowTexture);
    bg = Sprite(bgTexture);
    tile = Sprite(tileTexture);
    hole = Sprite(holeTexture);
    powerMeterFg = Sprite(powerMeterFgTexture);
    powerMeterBg = Sprite(powerMeterBgTexture);
    powerMeterOv = Sprite(powerMeterOvTexture);
    pauseScreenOverlay = Sprite(pauseScreenOverlayTexture);

    ball.setOrigin(ballTexture.getSize().x/2, ballTexture.getSize().y/2);
    ballShadow.setPosition(ball.getPosition().x+SHADOWS_OFFSET_X, ball.getPosition().y + SHADOWS_OFFSET_Y);
    ballShadow.setOrigin(ballShadowTexture.getSize().x/2, ballShadowTexture.getSize().y/2);
    arrow.setOrigin(arrowTexture.getSize().x/2, arrowTexture.getSize().y/2);
    powerMeterBg.setOrigin(powerMeterBgTexture.getSize().x/2, powerMeterBgTexture.getSize().y);
    powerMeterFg.setOrigin(powerMeterFgTexture.getSize().x/2, powerMeterFgTexture.getSize().y);
    powerMeterOv.setOrigin(powerMeterOvTexture.getSize().x/2, powerMeterOvTexture.getSize().y);
    hole.setOrigin(holeTexture.getSize().x/2, holeTexture.getSize().y/2);
    pauseScreenOverlay.setPosition(BORDER, BORDER);

}

void InitLevels() {

    GenerateLevel1();
    GenerateLevel2();
    GenerateLevel3();
    GenerateLevel4();

}

void GenerateLevel1() {

    // Level tiles
    tile.setPosition(-100,-100);
    currentLevel.push_back(tile);

    // Save Level
    levelTiles.push_back(currentLevel);
    currentLevel.resize(0);

    ballPosition = Vector2i(BORDER+3*TILE_SIZE_X+TILE_SIZE_X/2, BORDER+TILE_SIZE_Y*GRID_SIZE_Y/2);
    holePosition = Vector2i(BORDER+(GRID_SIZE_X-4)*TILE_SIZE_X+TILE_SIZE_X/2, BORDER+TILE_SIZE_Y*GRID_SIZE_Y/2);

    ballPositions.push_back(ballPosition);
    holePositions.push_back(holePosition);

}

void GenerateLevel2() {

    // Level tiles
    for(int i = 0; i < GRID_SIZE_Y/2+1; i++) {
        tile.setPosition(BORDER+GRID_SIZE_X/2*TILE_SIZE_X,BORDER+i*TILE_SIZE_Y);
        currentLevel.push_back(tile);
    }

    // Save Level
    levelTiles.push_back(currentLevel);
    currentLevel.resize(0);

    ballPosition = Vector2i(BORDER+3*TILE_SIZE_X+TILE_SIZE_X/2, BORDER+TILE_SIZE_Y*GRID_SIZE_Y/2);
    holePosition = Vector2i(BORDER+(GRID_SIZE_X-4)*TILE_SIZE_X+TILE_SIZE_X/2, BORDER+TILE_SIZE_Y*GRID_SIZE_Y/2);

    ballPositions.push_back(ballPosition);
    holePositions.push_back(holePosition);

}

void GenerateLevel3() {

    // Level tiles
    for(int i = 0; i < GRID_SIZE_Y/2+1; i++) {
        tile.setPosition(BORDER+GRID_SIZE_X/2*TILE_SIZE_X+2*TILE_SIZE_X,BORDER+i*TILE_SIZE_Y);
        currentLevel.push_back(tile);
    }
    for(int i = 0; i < GRID_SIZE_Y/2+1; i++) {
        tile.setPosition(BORDER+GRID_SIZE_X/2*TILE_SIZE_X-3*TILE_SIZE_X,BORDER+i*TILE_SIZE_Y);
        currentLevel.push_back(tile);
    }
    for(int i = 0; i < 4; i++) {
        tile.setPosition(BORDER+GRID_SIZE_X/2*TILE_SIZE_X+i*TILE_SIZE_X-2*TILE_SIZE_X,BORDER+GRID_SIZE_Y/2*TILE_SIZE_Y);
        currentLevel.push_back(tile);
    }

    // Save Level
    levelTiles.push_back(currentLevel);
    currentLevel.resize(0);

    ballPosition = Vector2i(BORDER+3*TILE_SIZE_X+TILE_SIZE_X/2, BORDER+TILE_SIZE_Y*3+TILE_SIZE_Y/2);
    holePosition = Vector2i(BORDER+(GRID_SIZE_X-4)*TILE_SIZE_X+TILE_SIZE_X/2, BORDER+TILE_SIZE_Y*3+TILE_SIZE_Y/2);

    ballPositions.push_back(ballPosition);
    holePositions.push_back(holePosition);

}

void GenerateLevel4() {

    // Level tiles
    for(int i = 0; i < GRID_SIZE_Y/2+2; i++) {
        tile.setPosition(BORDER+5*TILE_SIZE_X,BORDER+i*TILE_SIZE_Y);
        currentLevel.push_back(tile);
    }

    for(int i = GRID_SIZE_Y/2-1; i < GRID_SIZE_Y; i++) {
        tile.setPosition(WIDTH+BORDER-(6*TILE_SIZE_X),BORDER+i*TILE_SIZE_Y);
        currentLevel.push_back(tile);
    }

    tile.setPosition(WIDTH/2+BORDER,HEIGHT/2+BORDER-TILE_SIZE_Y/2);
    currentLevel.push_back(tile);
    tile.setPosition(WIDTH/2+BORDER-1*TILE_SIZE_X,HEIGHT/2+BORDER-TILE_SIZE_Y/2);
    currentLevel.push_back(tile);

    // Save Level
    levelTiles.push_back(currentLevel);
    currentLevel.resize(0);

    ballPosition = Vector2i(BORDER+2*TILE_SIZE_X+TILE_SIZE_X/2, BORDER+TILE_SIZE_Y*2+TILE_SIZE_Y/2);
    holePosition = Vector2i(BORDER+(GRID_SIZE_X-3)*TILE_SIZE_X+TILE_SIZE_X/2, BORDER+TILE_SIZE_Y*12+TILE_SIZE_Y/2);

    ballPositions.push_back(ballPosition);
    holePositions.push_back(holePosition);

}

void LoadLevel(int _level, int &_strokes) {

    // Load first level
    ball.setScale(1,1);
    ballShadow.setScale(1,1);
    ball.setPosition(ballPositions[_level].x, ballPositions[_level].y);
    hole.setPosition(holePositions[_level].x, holePositions[_level].y);
    ballShadow.setPosition(ball.getPosition().x+SHADOWS_OFFSET_X, ball.getPosition().y + SHADOWS_OFFSET_Y);

    _strokes = 0;

    UpdateTexts(_level, _strokes);

}

void LoadArcadeMode(int &_strokes, int _streak) {

    ball.setScale(1,1);
    ballShadow.setScale(1,1);
    ball.setPosition(BORDER+RandomInt(GRID_SIZE_X-1, 0)*TILE_SIZE_X+TILE_SIZE_X/2, BORDER+RandomInt(GRID_SIZE_Y-1, 0)*TILE_SIZE_Y+TILE_SIZE_Y/2);
    hole.setPosition(BORDER+RandomInt(GRID_SIZE_X-1, 0)*TILE_SIZE_X+TILE_SIZE_X/2, BORDER+RandomInt(GRID_SIZE_Y-1, 0)*TILE_SIZE_Y+TILE_SIZE_Y/2);
    ballShadow.setPosition(ball.getPosition().x+SHADOWS_OFFSET_X, ball.getPosition().y + SHADOWS_OFFSET_Y);

    _strokes = 0;
    UpdateTexts(_streak, _strokes, true);

    if(hole.getPosition().x == ball.getPosition().x && hole.getPosition().y == ball.getPosition().y) LoadArcadeMode(_strokes, _streak);

}

void UpdateArcadeMode(int _strokes, int &_lastHoleStrokes, int &_streak) {

    if(_strokes == _lastHoleStrokes+1) _streak+=1;
    else _streak = 1;

    _lastHoleStrokes = _strokes;

    ball.setScale(1,1);
    ballShadow.setScale(1,1);
    ball.setPosition(hole.getPosition().x, hole.getPosition().y);
    ballShadow.setPosition(ball.getPosition().x+SHADOWS_OFFSET_X, ball.getPosition().y+SHADOWS_OFFSET_Y);
    hole.setPosition(BORDER+RandomInt(GRID_SIZE_X-1, 0)*TILE_SIZE_X+TILE_SIZE_X/2, BORDER+RandomInt(GRID_SIZE_Y-1, 0)*TILE_SIZE_Y+TILE_SIZE_Y/2);

    UpdateTexts(_streak, _strokes, true);

    if(hole.getPosition().x == ball.getPosition().x && hole.getPosition().y == ball.getPosition().y) UpdateArcadeMode(_strokes, _lastHoleStrokes, _streak);

}

void UpdateTexts(int _levelStreak, int _strokes, bool _arcadeMode) {

    std::string msg;

    if(_arcadeMode) {

        msg = "Streak ";
        msg.append(std::to_string(_levelStreak));
    
    }
    else {
        
        msg = "Hole ";
        msg.append(std::to_string(_levelStreak+1));

    }
    guiTexts[0].setString(msg);
    guiTextsShadow[0].setString(msg);

    msg = "Strokes ";
    if(_strokes<=9) msg.append("0");
    msg.append(std::to_string(_strokes));
    guiTexts[1].setString(msg);
    guiTextsShadow[1].setString(msg);

    msg = "Difficulty ";
    msg.append(((difficulty == Difficulty::Easy) ? ("easy") : ((difficulty == Difficulty::Medium) ? ("medium") : ("hard"))));
    guiTexts[2].setString(msg);
    guiTextsShadow[2].setString(msg);

}

void UpdateClock(Clock _clock, int &_seconds, int &_minutes) {

    _seconds = ((int)_clock.getElapsedTime().asSeconds()) % 60;
    _minutes = (int)_clock.getElapsedTime().asSeconds() / 60;

    std::string clockString = "Time ";
    if(_minutes <= 9) clockString.append("0");
    clockString.append(std::to_string(_minutes) + ":");
    if(_seconds <= 9) clockString.append("0");
    clockString.append(std::to_string(_seconds));

    guiTexts[3].setString(clockString);
    guiTextsShadow[3].setString(clockString);

}

void BallIsMoving(Vector2i _mouseFinalPosition, Vector2i _initialBallPosition, float _deltaTime, Vector2i &_direction, float &_velocity, bool &_isMoving, int _level, bool &_inHole, int &_holeTime, Clock _clock, float &_holeAngle, Vector2i _fixedPoint) {

    // Calcolate the velocity and direction in which move the ball
    MoveTheBall(_mouseFinalPosition, _initialBallPosition, _deltaTime, _direction, _velocity, _isMoving, _fixedPoint);

    // Collision detection with screen bounds
    CalculateCollisionsWithScreenBounds(_direction);

    // Collision detection with tiles
    if(mode == Mode::Game) CalculateCollisionsWithTiles(_level, _direction);

    // Collision detection with hole
    CalculateCollisionsWithHole(_inHole, _holeTime, _clock, _velocity, _holeAngle);
    
    ballShadow.setPosition(ball.getPosition().x+SHADOWS_OFFSET_X, ball.getPosition().y + SHADOWS_OFFSET_Y);

}

void PressedOnTheBall(Vector2i &_initialBallPosition, bool &_isButtonDown) {

    _initialBallPosition = Vector2i(ball.getPosition().x, ball.getPosition().y);
    _isButtonDown = true;
    chargeSound.play();

    arrow.setPosition(ball.getPosition().x, ball.getPosition().y);
    if(ball.getPosition().x+POWERMETER_OFFSET_X+powerMeterBgTexture.getSize().x-BORDER > WIDTH) {

        powerMeterBg.setPosition(ball.getPosition().x-POWERMETER_OFFSET_X, ball.getPosition().y-POWERMETER_OFFSET_Y);
        powerMeterOv.setPosition(ball.getPosition().x-POWERMETER_OFFSET_X, ball.getPosition().y-POWERMETER_OFFSET_Y);
        powerMeterFg.setPosition(ball.getPosition().x-POWERMETER_OFFSET_X, ball.getPosition().y-POWERMETER_OFFSET_Y-(powerMeterBgTexture.getSize().y-powerMeterFgTexture.getSize().y)/2);

    }
    else {

        powerMeterBg.setPosition(ball.getPosition().x+POWERMETER_OFFSET_X, ball.getPosition().y-POWERMETER_OFFSET_Y);
        powerMeterOv.setPosition(ball.getPosition().x+POWERMETER_OFFSET_X, ball.getPosition().y-POWERMETER_OFFSET_Y);
        powerMeterFg.setPosition(ball.getPosition().x+POWERMETER_OFFSET_X, ball.getPosition().y-POWERMETER_OFFSET_Y-(powerMeterBgTexture.getSize().y-powerMeterFgTexture.getSize().y)/2);

    }

}

void PressedOnTheDifficultyText() {

    difficulty = ((difficulty == Difficulty::Easy) ? (Difficulty::Medium) : ((difficulty == Difficulty::Medium) ? (Difficulty::Hard) : (Difficulty::Easy)));
    guiTexts[2].setColor(((difficulty == Difficulty::Easy) ? (colorEasy) : ((difficulty == Difficulty::Medium) ? (colorMedium) : (colorHard))));
    
    std::string difficultyString = "Difficulty ";
    difficultyString.append(((difficulty == Difficulty::Easy) ? ("easy") : ((difficulty == Difficulty::Medium) ? ("medium") : ("hard"))));
    guiTexts[2].setString(difficultyString);
    guiTextsShadow[2].setString(difficultyString);

    clickSound.play();

}

void ReleaseFromTheBall(bool &_isButtonDown, Vector2i &_mouseFinalPosition, Vector2i &_mousePosition, Vector2i &_direction, bool &_isMoving, float &_velocity, int &_strokes, bool _muteSfx, Vector2i _initialBallPosition, Vector2i &_fixedPoint) {

    _isButtonDown = false;

    if(_velocity > 10) {

        _mouseFinalPosition = _mousePosition;
        _isMoving = true;
        _direction = Vector2i(1,1);
        swingSound.setVolume(_velocity/MAX_VELOCITY*SFX_VOLUME);
        if(!_muteSfx) swingSound.play();

        _strokes+=1;
        
        std::string strokesString = "Strokes ";
        if(_strokes<=9) strokesString.append("0");
        strokesString.append(std::to_string(_strokes));
        guiTexts[1].setString(strokesString);
        guiTextsShadow[1].setString(strokesString);

        if(GetDistance(_mouseFinalPosition, _initialBallPosition) > MAX_VELOCITY) {

            _fixedPoint.x = _initialBallPosition.x + sin((GetAngle(_mouseFinalPosition, _initialBallPosition)-90+180)*3.141/180) * MAX_VELOCITY;
            _fixedPoint.y = _initialBallPosition.y + cos((GetAngle(_mouseFinalPosition, _initialBallPosition)-90)*3.141/180) * MAX_VELOCITY;

        }
        else {
        
            _fixedPoint.x = _mouseFinalPosition.x;
            _fixedPoint.y = _mouseFinalPosition.y;
        
        }

    }

}

void HoldingTheButton(float &_velocity, Vector2i _mousePosition, Vector2i _initialBallPosition) {

    _velocity = GetDistance(_mousePosition, _initialBallPosition);

    if(_velocity > MAX_VELOCITY) _velocity = MAX_VELOCITY;
    
    arrow.setRotation(GetAngle(_mousePosition, Vector2i(ball.getPosition().x, ball.getPosition().y)) + 90 + 180);
    powerMeterFg.setScale(1, _velocity/MAX_VELOCITY);

}

void MoveTheBall(Vector2i _mouseFinalPosition, Vector2i _initialBallPosition, float _deltaTime, Vector2i &_direction, float &_velocity, bool &_isMoving, Vector2i _fixedPoint) {

    Vector2i deltaPos;
    Vector2f velocityVector;

    deltaPos.x = (_initialBallPosition.x-_fixedPoint.x) * _direction.x;
    deltaPos.y = (_initialBallPosition.y-_fixedPoint.y) * _direction.y;

    velocityVector.x = deltaPos.x * _velocity * _deltaTime;
    velocityVector.y = deltaPos.y * _velocity * _deltaTime;

    if(_velocity < 0) _velocity = 0;
    ball.move(velocityVector.x * _deltaTime * BALL_VELOCITY, velocityVector.y * _deltaTime * BALL_VELOCITY);

    if(_velocity == 0) _isMoving = false;

    _velocity -= FRICTION * _deltaTime;

}

void CalculateCollisionsWithScreenBounds(Vector2i &_direction) {

    if(ball.getPosition().x < BORDER+ballTexture.getSize().x/2) { 

        ball.setPosition(BORDER+ballTexture.getSize().x/2, ball.getPosition().y);
        _direction.x *= -1;

    }
    else if(ball.getPosition().x > (WIDTH+2*BORDER)-BORDER-ballTexture.getSize().x/2) {

        ball.setPosition((WIDTH+2*BORDER)-BORDER-ballTexture.getSize().x/2, ball.getPosition().y);
        _direction.x *= -1;
    }
    
    if(ball.getPosition().y < BORDER+ballTexture.getSize().y/2) {

        ball.setPosition(ball.getPosition().x, BORDER+ballTexture.getSize().y/2);
        _direction.y *= -1;

    }
    else if(ball.getPosition().y > (HEIGHT+2*BORDER)-BORDER-ballTexture.getSize().y/2) {

        ball.setPosition(ball.getPosition().x, (HEIGHT+2*BORDER)-BORDER-ballTexture.getSize().y/2);
        _direction.y *= -1;

    }

}

void CalculateCollisionsWithTiles(int _level, Vector2i &_direction) {

    for(Sprite tile : levelTiles[_level]) {

        if(tile.getGlobalBounds().intersects(ball.getGlobalBounds())) {

            int distanceX = DistanceToLine(Vector2i(tile.getPosition().x + TILE_SIZE_X / 2, tile.getPosition().y + TILE_SIZE_Y / 2), Vector2i(tile.getPosition().x + TILE_SIZE_X, tile.getPosition().y + TILE_SIZE_Y / 2), Vector2i(ball.getPosition().x, ball.getPosition().y));
            int distanceY = DistanceToLine(Vector2i(tile.getPosition().x + TILE_SIZE_X / 2, tile.getPosition().y + TILE_SIZE_Y / 2), Vector2i(tile.getPosition().x + TILE_SIZE_X / 2, tile.getPosition().y + TILE_SIZE_Y), Vector2i(ball.getPosition().x, ball.getPosition().y));

            if(abs(distanceX) < abs(distanceY)) {

                if(distanceY < 0) { // Left

                    ball.setPosition(tile.getPosition().x-ballTexture.getSize().x/2, ball.getPosition().y);
                    _direction.x *= -1;

                }
                else {  // Right

                    ball.setPosition(tile.getPosition().x+ballTexture.getSize().x/2+TILE_SIZE_X, ball.getPosition().y);
                    _direction.x *= -1;

                }

            }
            else {

                
                if(distanceX >= -23)
                if(distanceX < 0) { // Bottom

                    ball.setPosition(ball.getPosition().x, tile.getPosition().y+ballTexture.getSize().y/2+TILE_SIZE_Y);
                    _direction.y *= -1;

                }
                else if(distanceX > 0) { // Top

                    ball.setPosition(ball.getPosition().x, tile.getPosition().y-ballTexture.getSize().y/2);
                    _direction.y *= -1;

                }

            }

        }

    }

}

void CalculateCollisionsWithHole(bool &_inHole, int &_holeTime, Clock _clock, float &_velocity, float &_holeAngle) {

    if(hole.getGlobalBounds().intersects(ball.getGlobalBounds()) && _velocity <= VELOCITY_LIMIT_TO_HOLE) {

        // Level up
        if(!_inHole) {

            _inHole = true;
            _holeTime = _clock.getElapsedTime().asSeconds();

            holeSound.play();

            _velocity = 0;
            _holeAngle = GetAngle(Vector2i(ball.getPosition().x, ball.getPosition().y), Vector2i(hole.getPosition().x, hole.getPosition().y)) * 3.141 / 180;

        }

    }

}

void BallAnimation(float _deltaTime, float _holeAngle) {

    float ballToHoleSpeed = -GetDistance(Vector2i(ball.getPosition().x, ball.getPosition().y), Vector2i(hole.getPosition().x, hole.getPosition().y));

    ball.move(_deltaTime * ballToHoleSpeed * cos(_holeAngle), _deltaTime * ballToHoleSpeed * sin(_holeAngle));
    ballShadow.move(_deltaTime * ballToHoleSpeed * cos(_holeAngle), _deltaTime * ballToHoleSpeed * sin(_holeAngle));
    if(ball.getScale().x > 0) ball.setScale(ball.getScale().x-1*_deltaTime/HOLE_ANIMATION_TIME, ball.getScale().y-1*_deltaTime/HOLE_ANIMATION_TIME);
    if(ballShadow.getScale().x > 0) ballShadow.setScale(ballShadow.getScale().x-1*_deltaTime/HOLE_ANIMATION_TIME, ballShadow.getScale().y-1*_deltaTime/HOLE_ANIMATION_TIME);

}

void OverlayText(Vector2i _mousePosition) {

    if(mode == Mode::Menu)
        for(Text &text : menuTexts) {

            if(text.getGlobalBounds().contains(_mousePosition.x, _mousePosition.y)) text.setColor(colorOverlayedText);

            else text.setColor(colorText);

        }
    else if(mode == Mode::Pause)
        for(Text &text : pauseTexts) {

            if(text.getGlobalBounds().contains(_mousePosition.x, _mousePosition.y)) text.setColor(colorOverlayedText);

            else text.setColor(colorText);

        }
    else if(mode == Mode::EndGame)
        for(int i = 2; i < endGameTexts.size(); i++) {

            if(endGameTexts[i].getGlobalBounds().contains(_mousePosition.x, _mousePosition.y)) endGameTexts[i].setColor(colorOverlayedText);

            else endGameTexts[i].setColor(colorText);

        }

}

void UpdateRender(int _level, bool isButtonDown, bool _isMoving, Mode _lastMode) {

    window.clear();
    window.draw(bg);
    
   if(mode == Mode::Game || mode == Mode::Pause || mode == Mode::Arcade) {

        if(_level < levelTiles.size()) {

            if(mode != Mode::Arcade && _lastMode != Arcade) for(Sprite tile : levelTiles[_level]) window.draw(tile);

            window.draw(hole);

            if(isButtonDown && !_isMoving) {

                if(difficulty == Difficulty::Easy || difficulty == Difficulty::Medium) window.draw(arrow);
                if(difficulty == Difficulty::Easy) {
                
                    window.draw(powerMeterBg);
                    window.draw(powerMeterFg);
                    window.draw(powerMeterOv);
                
                }
            }

            window.draw(ballShadow);
            window.draw(ball);

            for(Text text : guiTextsShadow) window.draw(text);
            for(Text text : guiTexts) window.draw(text);

            if(mode == Mode::Pause) {
            
                window.draw(pauseScreenOverlay);
                for(Text text : pauseTextsShadow) window.draw(text);
                for(Text text : pauseTexts) window.draw(text);

            }

        }
        else {

            mode = Mode::EndGame;

        }

    }
    
    else if(mode == Mode::Menu) {

        for(Text text : menuTextsShadow) window.draw(text);
        for(Text text : menuTexts) { window.draw(text); }

    }

    else if(mode == Mode::EndGame) {

        for(Text text : endGameTextsShadows) window.draw(text);
        for(Text text : endGameTexts) window.draw(text);

    }

    else if(mode == Mode::Options) {

        for(Text text : OptionsTextsShadows) window.draw(text);
        for(Text text : OptionsTexts) window.draw(text);

    }
    
    window.display();

}

int RandomInt(int _max, int _min) {

    return rand() % (_max - _min + 1) + _min;

}

float GetDistance(Vector2i _vector1, Vector2i _vector2) {

    return sqrtl(pow(_vector1.x-_vector2.x, 2) + pow(_vector1.y-_vector2.y, 2));

}

float GetAngle(Vector2i _vector1, Vector2i _vector2) {

    int deltaX = _vector1.x - _vector2.x;
    int deltaY = _vector1.y - _vector2.y;

    float angleInDegrees = atan2(deltaY, deltaX) * 180 / 3.141;

    return angleInDegrees;

}

float DistanceToLine(Vector2i line_start, Vector2i line_end, Vector2i point)
{

	double normalLength = hypot(line_end.x - line_start.x, line_end.y - line_start.y);
	double distance = (double)((point.x - line_start.x) * (line_end.y - line_start.y) - (point.y - line_start.y) * (line_end.x - line_start.x)) / normalLength;
	return distance;

}