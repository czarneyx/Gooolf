# Gooolf
A simple mini-golf game

# About the project
## General information
### Initialization
At the top of the code there are the declarations of some constants and global variables that will be frequently used in the code by functions and procedures, followed by the declaration of the functions and procedures divided in the following order:
- Initialization procedures (which will assign a value to many of the global variables)
- Level generation
- Initialization texts in different menus
- Procedures useful to the code
- Math functions
### main
At the start of the main function some local variables are declared and then is called the Init() procedure, that like said before will initialize all the global variables need trough the program.
At this point the program loop begins.

### Window loop
Every frame the window search for events (an event defines a system event and its parameters. Event holds all the information about a system event that just happened.) and check if that event is linked to a function in the program.
Then check if other conditions are respected to update some procedures that don't need an event to be executed.
After all that update the object that are been rendered

## Ball movements

When the left key of the mouse is pressed the program checks if the game is in play mode, if so it also check if the mouse position is in the ball bounds and if the ball is not moving, if also this conditions are respected:
- set the initial ball position to the actual ball position
- place the arrow and the power meter close to the ball and 
- set the left mouse button as pressed.

While the button is pressed the program calculate the velocity of the ball and update the angle in witch is aiming the arrow as the value in the power meter slider.

When the left mouse key is released it checks if we are playing, if the button was down and if the ball wasn't moving, if so it set the button as not pressed and if the speed is > 10 (that means that the mouse was outside the ball and the user hasn't just pressed on the ball to release instantly):
- Set the mouse final position as the mouse position 
- The ball as it is moving
- Increase the number of strokes made and update the GUI text
- Set the swing sound volume depending on the ball speed (MaxVolume(100) : MaxVelocity = X : Velocity) and play it
- Calculate a fixed point between the ball position and the mouse final position at the distance from the ball equal to the max velocity if major than it

Every frame that the ball is moving:
- calculate the direction to move the ball divided on the two axis 
- calculate the movement based on the velocity
- move the ball of the movement calculated before
- decrease the velocity by a constant friction
- if the velocity is equal to 0 stop moving the ball 

after that it start checking for collisions with screen, tiles and the hole and if it needs to reverse the direction of the ball 

![image](https://cdn.discordapp.com/attachments/968442315586830356/969977761320038410/BallMovementMath.png)

# Prerequisites
- Download and install a [MinGW](https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/7.3.0/threads-posix/dwarf/i686-7.3.0-release-posix-dwarf-rt_v5-rev0.7z/download") version compatible with [SFML 2.5.1](https://www.sfml-dev.org/download/sfml/2.5.1/) to compile the project

## Executable
You can download the executable version for windows from the following [link](https://drive.google.com/uc?export=download&id=132TjY2NZnA2StvTrYu-lBjhrvD0kKoYB)